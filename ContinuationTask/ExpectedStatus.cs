﻿namespace ContinuationTask
{
    public enum ExpectedStatus
    {
        RanToCompletion,
        Faulted,
        Canceled
    }
}
