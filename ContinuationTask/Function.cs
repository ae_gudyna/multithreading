﻿using System.Threading;

namespace ContinuationTask
{
    public static class Function
    {
        public static int Sum(int n)
        {
            int sum = 0;
            for (; n > 0; n--)
            {
                checked { sum += n; }
            }
            return sum;
        }
        public static int Sum(int n, CancellationToken ct)
        {
            int sum = 0;
            for (; n > 0; n--)
            {
                ct.ThrowIfCancellationRequested();
                checked { sum += n; }
                Thread.Sleep(100);
            }
            return sum;
        }
    }
}
