﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ContinuationTask
{
    public class TaskBuilder
    {

        public static Task<int> CreateTask(CancellationTokenSource cts, ExpectedStatus status)
        {
            return new Task<int>(() =>
            {
                Console.WriteLine($"Start of Task with thread id {Thread.CurrentThread.ManagedThreadId}, expected status: {Enum.GetName(typeof(ExpectedStatus), status)}");
                int result;
                if (status == ExpectedStatus.RanToCompletion)
                {
                    result = Function.Sum(10000);
                }
                else if (status == ExpectedStatus.Canceled)
                {
                    result = Function.Sum(10000, cts.Token);
                }
                else
                {
                    result = Function.Sum(100000);
                }

                Console.WriteLine($"End of Task with thread id {Thread.CurrentThread.ManagedThreadId}");
                return result;

            }, cts.Token);
        }

        public static void CreateTaskWithContinuation(CancellationTokenSource cts, ExpectedStatus status, Action<Task<int>> action, TaskContinuationOptions option)
        {
            var successTask = CreateTask(cts, status);
            successTask.ContinueWith(action, option);
            successTask.Start();
        }
    }
}
