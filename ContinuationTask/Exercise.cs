﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ContinuationTask
{
    public class Exercise
    {

        private static string PrintExercize()
        {
            var exercize = new StringBuilder();
            exercize.Append("Chooze variant:");
            exercize.Append(Environment.NewLine);
            exercize.Append("a. Continuation task should be executed regardless of the result of the parent task.");
            exercize.Append(Environment.NewLine);
            exercize.Append("b. Continuation task should be executed when the parent task finished without success.");
            exercize.Append(Environment.NewLine);
            exercize.Append("c. Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation.");
            exercize.Append(Environment.NewLine);
            exercize.Append("d. Continuation task should be executed outside of the thread pool when the parent task would be cancelled");
            exercize.Append(Environment.NewLine);
            return exercize.ToString();
        }

        private static void Case(Action<Task<int>> action, TaskContinuationOptions option)
        {
            var cts = new CancellationTokenSource();
            TaskBuilder.CreateTaskWithContinuation(cts, ExpectedStatus.RanToCompletion, action, option);

            TaskBuilder.CreateTaskWithContinuation(cts, ExpectedStatus.Faulted, action, option);

            TaskBuilder.CreateTaskWithContinuation(cts, ExpectedStatus.Canceled, action, option);
            Thread.Sleep(500);
            cts.Cancel();
        }

        private static void CaseA()
        {
            Case(task => Task.Run(() => Console.WriteLine($"ContinuationTask Default. Thread id: {Thread.CurrentThread.ManagedThreadId}. Result: {task.Status}")),
                TaskContinuationOptions.None);
        }

        private static void CaseB()
        {
            Case(task => Task.Run(() => Console.WriteLine($"ContinuationTask Not Succeed. Thread id: {Thread.CurrentThread.ManagedThreadId}. Result: {task.Status}")),
                TaskContinuationOptions.NotOnRanToCompletion);
        }

        private static void CaseC()
        {
            Case(task => new Task(() =>
                        Console.WriteLine($"ContinuationTask With Fail. Thread id: {Thread.CurrentThread.ManagedThreadId}. Result: {task.Status}"),
                    TaskCreationOptions.AttachedToParent).Start(),
                TaskContinuationOptions.OnlyOnFaulted);
        }
        private static void CaseD()
        {
            Case(task => new Task(() =>
                        Console.WriteLine($"ContinuationTask Long Running. Thread id: {Thread.CurrentThread.ManagedThreadId}. Result: {task.Status}"),
                    TaskCreationOptions.LongRunning).Start(),
                TaskContinuationOptions.OnlyOnCanceled);
        }

        public static void Run()
        {
            Console.WriteLine(PrintExercize());

            while (true)
            {
                var variant = Console.ReadLine();

                if(variant == "a")
                    CaseA();
                else if(variant == "b")
                    CaseB();
                else if (variant == "c")
                    CaseC();
                else if (variant == "d")
                    CaseD();
                else
                {
                    Console.WriteLine("Exit the program");
                    break;
                }
            }
        }
    }
}