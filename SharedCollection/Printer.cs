﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace SharedCollection
{
    public class Printer
    {
        private readonly IList collection;
        private readonly ReadWriteLock simpleLock;
        public Printer(ReadWriteLock simpleLock, IList collection)
        {
            this.simpleLock = simpleLock;
            this.collection = collection;
        }

        public Task ReadCollection()
        {
            return Task.Run(() =>
            {
                while (true)
                {
                    simpleLock.Enter(TypeOfThread.PrintThread);
                    Console.WriteLine(collection[collection.Count - 1]);
                    simpleLock.Leave(TypeOfThread.PrintThread);
                }
            });
        }
    }
}
