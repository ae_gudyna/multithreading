﻿using System.Collections;
using System.Threading.Tasks;

namespace SharedCollection
{
    public class Writer
    {
        private IList collection;
        private readonly ReadWriteLock simpleLock;
        public Writer(ReadWriteLock simpleLock, IList collection)
        {
            this.collection = collection;
            this.simpleLock = simpleLock;
        }

        private void Add(object value)
        {
            
            simpleLock.Enter(TypeOfThread.WritingThread);
            collection.Add(value);
            simpleLock.Leave(TypeOfThread.WritingThread);
        }
        public Task GenerateCollection(int sizeOfElements)
        {
            return Task.Run(() =>
            {
                for (var i = 0; i < sizeOfElements; i++)
                {
                    Add(i);
                }
            });
        }
    }
}
