﻿using System;
using System.Collections.Generic;

namespace SharedCollection
{
    class Program
    {
        private static readonly int sizeOfArray = 10;
        static void Main(string[] args)
        {
            var simpleLock = new ReadWriteLock();
            var array = new List<int>(sizeOfArray);
            var printer = new Printer(simpleLock, array);
            var adder = new Writer(simpleLock, array);

            printer.ReadCollection();
            adder.GenerateCollection(sizeOfArray);

            Console.ReadLine();
        }

    }
}
