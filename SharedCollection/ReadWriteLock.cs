﻿using System;
using System.Threading;

namespace SharedCollection
{
    public enum TypeOfThread
    {
        WritingThread,
        PrintThread
    }
    public sealed class ReadWriteLock : IDisposable
    {
        public int needToPrint = 0;
        private int m_writeWaiters = 0;
        private int m_printWaiters = 0;
        private AutoResetEvent m_waiterLock = new AutoResetEvent(false);
        public void Enter(TypeOfThread typeOfThread)
        {
            if (typeOfThread == TypeOfThread.PrintThread)
            {
                if (needToPrint == (int)typeOfThread)
                    return;
                Interlocked.Increment(ref m_printWaiters);
                m_waiterLock.WaitOne();
            }
            else
            {
                if (needToPrint == (int)typeOfThread &  Interlocked.Increment(ref m_writeWaiters) == 1)
                    return;
                m_waiterLock.WaitOne();
            }
        }
        public void Leave(TypeOfThread typeOfThread)
        {
            if (typeOfThread == TypeOfThread.PrintThread)
            {
                Interlocked.Decrement(ref needToPrint);
                if (Interlocked.Decrement(ref m_printWaiters) == 0 && m_writeWaiters == 0)
                    return;
                m_waiterLock.Set();
            }
            else
            {
                Interlocked.Increment(ref needToPrint);
                if (Interlocked.Decrement(ref m_writeWaiters) == 0 && m_printWaiters == 0)
                    return;
                m_waiterLock.Set();
            }
        }
        public void Dispose() { m_waiterLock.Dispose(); }
    }
}
