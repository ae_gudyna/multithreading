﻿using FourTasks.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FourTasks
{
    [Logger]
    public class TaskBuilder
    {
        public Task<TResult> NewTask<T, TResult>(Func<T, TResult> function, string message, T value)
        {
            return Task.Run(() =>
            {
                var result = function(value);
                WriteValue(message, result);
                Console.WriteLine();
                return result;
            });
        }

        public Task<TResult> NewTask<TResult>(Func<TResult> function, string message)
        {
            return Task.Run(() =>
            {
                var result = function();
                WriteValue(message, result);
                Console.WriteLine();
                return result;
            });
        }
        
        private void WriteValue<T>(string action, T value)
        {
            if (value is IEnumerable array)
            {
                foreach (var number in array) Console.WriteLine($"{action}: {number.ToString()}");
            }
            else
            {
                Console.WriteLine($"{action}: {value.ToString()}");
            }
        }
    }
}
