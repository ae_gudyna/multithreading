﻿using FourTasks.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FourTasks
{
    [Logger]
    public class WorkWithArray
    {
        private readonly int sizeOfArray;
        private readonly int minRandomValue;
        private readonly int maxRandomValue;
        private readonly TaskBuilder taskBuilder = new TaskBuilder();
        private readonly Random random = new Random();

        public WorkWithArray(int sizeOfArray, int minRandomValue, int maxRandomValue)
        {
            this.sizeOfArray = sizeOfArray;
            this.minRandomValue = minRandomValue;
            this.maxRandomValue = maxRandomValue;
        }
        public void Run()
        {
            taskBuilder.NewTask(CreateArray, "Create Array", sizeOfArray).ContinueWith(task =>
            {
                taskBuilder.NewTask(MultiplyArray, "Multiply", task.Result).ContinueWith(mTask =>
                {
                    taskBuilder.NewTask(AscendingSort, "Ascending", mTask.Result).ContinueWith(ascTask =>
                    {
                        taskBuilder.NewTask(AvgOfArray, "Avg", ascTask.Result);
                    });
                });
            });
        }

        IEnumerable<int> CreateArray(int sizeOfArray)
        {
            var array = new List<int>(sizeOfArray);
            for (var i = 0; i < sizeOfArray; i++)
            {
                array.Add(GetRandomValue());
            }
            return array;
        }

        IEnumerable<int> MultiplyArray(IEnumerable<int> array)
        {
            var multiplyingArray = new List<int>(sizeOfArray);
            foreach (var number in array)
            {
                multiplyingArray.Add(number * GetRandomValue());
            }
            return multiplyingArray;
        }

        IEnumerable<int> AscendingSort(IEnumerable<int> array)
        {
            return array.OrderBy(x => x);
        }

        double AvgOfArray(IEnumerable<int> array)
        {
            return array.Average();
        }

        int GetRandomValue()
        {
            return random.Next(minRandomValue, maxRandomValue);
        }
    }
}
