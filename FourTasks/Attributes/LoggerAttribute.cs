﻿using Newtonsoft.Json;
using PostSharp.Aspects;
using PostSharp.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FourTasks.Attributes
{
    [PSerializable]
    public class LoggerAttribute : OnMethodBoundaryAspect
    {
        private static bool loggerFolderExists = false;
        private string folderForLogs;
        private string logFile;
        private static object obj = new object();

        public LoggerAttribute() : base()
        {
            folderForLogs = "C:\\Logging";
            logFile = folderForLogs + "\\log.txt";
            if (!loggerFolderExists)
            {
                if (!Directory.Exists(folderForLogs))
                {
                    Directory.CreateDirectory(folderForLogs);
                }
                loggerFolderExists = true;
            }

        }
        public override bool CompileTimeValidate(MethodBase method)
        {
            return method.MemberType == MemberTypes.Method;
        }
        public override void OnEntry(MethodExecutionArgs args)
        {
                WriteLogs($"execution of method: {args.Method.Name}({GetArguments(args.Arguments)})");
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            var returnedValueText = args.ReturnValue != null ? $"with returned value {TrySerializeObject(args.ReturnValue)}." : "";
            WriteLogs($"method {args.Method.Name}({GetArguments(args.Arguments)}) executed successfully {returnedValueText}");
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            WriteLogs($"method {args.Method.Name}({GetArguments(args.Arguments)}) has exited.");
        }

        public override void OnException(MethodExecutionArgs args)
        {
            WriteLogs($"exception was thrown in method {args.Method.Name}({GetArguments(args.Arguments)})");
        }

        private string GetArguments(Arguments args)
        {
            
             return string.Join(";", args.AsEnumerable().Select(x => TrySerializeObject(x)));
        }

        private void WriteLogs(string log)
        {
            lock (obj)
            {
                using (StreamWriter writer = new StreamWriter(logFile, true))
                {
                    writer.WriteLine($"({DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")}) {log}");
                    writer.Flush();
                }
            }
        }
        private string TrySerializeObject(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj);
            }
            catch
            {
                return "Not serializable";
            }
        }
    }
}
